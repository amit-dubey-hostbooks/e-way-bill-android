import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController
} from "ionic-angular";
import { TranslateService } from "./../../providers/translate";
import { CommonService } from "./../../providers/common";
import { ApiService } from "./../../providers/api";

@IonicPage()
@Component({
  selector: "page-dashboard",
  templateUrl: "dashboard.html"
})
export class DashboardPage {
  firstActive: boolean = true;
  secondActive: boolean = false;
  thirdActive: boolean = false;
  fourthActive: boolean = false;
  ewbInput: boolean = true;
  ewbGenerated: boolean = false;
  displayAddBillButton: boolean = true;

  bills: any = [];
  inputBillsList: any = [];
  tempInputBillsList: any = [];
  generatedBillsList: any = [];
  tempGeneratedBillsList: any = [];
  items: any = [];
  tempItems: any = [];
  clients: any = [];
  tempClients: any = [];
  gstinRegxp = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public api: ApiService,
    public common: CommonService
  ) { }

  ionViewDidEnter() {
    this.inputBillList();
  }

  inputBillList() {
    this.firstActive = true;
    this.secondActive = false;
    this.thirdActive = false;

    this.ewbInput = true;
    this.ewbGenerated = false;
    this.displayAddBillButton = true;

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let isaction = "N";
    this.common.displayLoader("Please wait...");
    this.api
      .get(`get-ewb-list/${userid}/${buid}/${gstinid}/${isaction}`)
      .subscribe(
        res => {
          this.common.hideLoader();
          if (res["status"] == false) {
            this.common.displayToaster(res["message"]);
          } else {
            this.inputBillsList = JSON.parse(atob(res["ewbList"]));
            this.tempInputBillsList = JSON.parse(atob(res["ewbList"]));
            console.log(this.inputBillsList);
          }
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  generatedBillList() {
    this.firstActive = true;
    this.secondActive = false;
    this.thirdActive = false;

    this.ewbInput = false;
    this.ewbGenerated = true;

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let isaction = "Y";
    this.common.displayLoader("Please wait...");
    this.api
      .get(`get-ewb-list/${userid}/${buid}/${gstinid}/${isaction}`)
      .subscribe(
        res => {
          this.common.hideLoader();
          if (res["status"] == false) {
            this.common.displayToaster(res["message"]);
          } else {
            this.generatedBillsList = JSON.parse(atob(res["ewbList"]));
            this.tempGeneratedBillsList = JSON.parse(atob(res["ewbList"]));
            console.log(this.generatedBillsList);
          }
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  searchBills(event) {
    this.initializeBills();
    const val = event.target.value;
    if (this.ewbGenerated) {
      if (val && val.trim() != "") {
        this.generatedBillsList = this.generatedBillsList.filter(bill => {
          return (
            bill.fromTrdName.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.toTrdName.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.supplyTypeDesc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.docNo.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.ewayBillNo.toLowerCase().indexOf(val.toLowerCase()) > -1
          );
        });
      }
    } else {
      if (val && val.trim() != "") {
        this.inputBillsList = this.inputBillsList.filter(bill => {
          return (
            bill.fromTrdName.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.toTrdName.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.supplyTypeDesc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            bill.docNo.toLowerCase().indexOf(val.toLowerCase()) > -1
          );
        });
      }
    }
  }

  initializeBills() {
    if (this.ewbGenerated) {
      this.generatedBillsList = this.tempGeneratedBillsList;
    } else {
      this.inputBillsList = this.tempInputBillsList;
    }
  }

  itemsList() {
    this.firstActive = false;
    this.secondActive = true;
    this.thirdActive = false;
    this.displayAddBillButton = false;

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let itemid = 0;
    let isaction = "L";
    this.common.displayLoader("Please wait...");
    this.api
      .get(`getitemmster/${userid}/${buid}/${gstinid}/${itemid}/${isaction}`)
      .subscribe(
        res => {
          console.log(res);
          if (res["status"] == false) {
            this.items = [];
            this.common.displayToaster(res["message"]);
          } else {
            this.items = JSON.parse(atob(res["itemList"]));
            this.tempItems = JSON.parse(atob(res["itemList"]));
            localStorage.setItem("itemList", atob(res["itemList"]));
          }
          console.log(this.items);
          this.common.hideLoader();
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  searchItem(event) {
    this.initializeItems();
    const val = event.target.value;
    if (val && val.trim() != "") {
      this.items = this.items.filter(item => {
        console.log(item);
        return (
          item.item_description.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.hsn_sac_code.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.item_sku_code.toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      });
    }
  }

  initializeItems() {
    this.items = this.tempItems;
  }

  clientsList() {
    this.firstActive = false;
    this.secondActive = false;
    this.thirdActive = true;
    this.displayAddBillButton = false;

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let clientid = 0;
    let isaction = "L";
    this.common.displayLoader("Please wait...");
    this.api
      .get(`getclient/${userid}/${buid}/${gstinid}/${clientid}/${isaction}/`)
      .subscribe(
        res => {
          if (res["status"] == false) {
            this.clients = [];
            this.common.displayToaster(res["message"]);
          } else {
            this.clients = JSON.parse(atob(res["client"]));
            this.tempClients = JSON.parse(atob(res["client"]));
            localStorage.setItem("clientList", atob(res["client"]));
          }
          console.log(this.clients);
          this.common.hideLoader();
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  searchClient(event) {
    this.initializeClients();
    const val = event.target.value;
    if (val && val.trim() != "") {
      this.clients = this.clients.filter(client => {
        return (
          client.clientname.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          client.state_name.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          client.gstin.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          client.pan.toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      });
    }
  }

  initializeClients() {
    this.clients = this.tempClients;
  }

  addEwayBill() {
    let items = JSON.parse(localStorage.getItem("itemList"));
    let clients = JSON.parse(localStorage.getItem("clientList"));
    if (items.length == 0) {
      this.common.displayToaster(`You haven't added any item yet!`);
      let data = {
        button_text: "Add Item",
        itemData: null
      };
      let addItemModal = this.modalCtrl.create("ItemDetailsPage", data);
      addItemModal.onDidDismiss(data => {
        this.itemsList();
      });
      addItemModal.present();
    } else if (clients.length == 0) {
      this.common.displayToaster(`You haven't added any client yet!`);
      let data = {
        button_text: "Add Client",
        clientData: null
      };
      let addCustomerModal = this.modalCtrl.create("ClientDetailsPage", data);
      addCustomerModal.onDidDismiss(data => {
        console.log(data);
        this.clientsList();
      });
      addCustomerModal.present();
    } else {
      this.navCtrl.push("AddBillPage");
    }
  }

  ewayBillLogin() {
    return new Promise((resolve, reject) => {
      let tokenSaveTime = localStorage.getItem("tokenSaveTime");
      let timeDiff = 1000;
      if (tokenSaveTime != null && tokenSaveTime != undefined) {
        let savedTime = new Date(JSON.parse(tokenSaveTime));
        let currentTime = new Date();
        timeDiff = Math.round(
          (currentTime.getTime() - savedTime.getTime()) / 1000 / 60
        );
      }
      if ((tokenSaveTime != null) && (tokenSaveTime != undefined) && (timeDiff <= 360)) {
        this.common.displayToaster('E-Way Bill LoggedIn Successfully!');
        resolve(true);
      } else {
        const prompt = this.alertCtrl.create({
          title: "NSDL Login Credentials",
          inputs: [
            {
              name: "username",
              type: "text",
              value: "Subhash050_API_hbl",
              placeholder: "Username/GSTIN"
            },
            {
              name: "password",
              type: "password",
              value: "Admin@12345678",
              placeholder: "Password"
            }
          ],
          buttons: [
            {
              text: "Cancel",
              cssClass: "cancelAlertButton",
              handler: data => {
                resolve(false);
              }
            },
            {
              text: "Login",
              cssClass: "submitAlertButton",
              handler: data => {
                if (!data.username) {
                  this.common.displayToaster("Username/GSTIN is required!");
                  return false;
                }
                if (!data.password) {
                  this.common.displayToaster("Password is required!");
                  return false;
                }
                else {
                  let userData = {
                    userid: this.common.loginData.ID,
                    buid: this.common.selectedBusiness.buid,
                    gstinid: this.common.selectedBusiness.GSTIN.gstinid,
                    ewbUserID: data.username,
                    ewbPassword: data.password
                  };
                  console.log(userData);
                  this.common.displayLoader("Please wait...");
                  this.api.post("generate-token", userData).subscribe((res) => {
                    console.log(res);
                    this.common.hideLoader();
                    if (res["isSuccess"] == false) {
                      this.common.displayToaster(res["error"]);
                      resolve(false);
                    } else {
                      let newDate = new Date();
                      localStorage.setItem("tokenSaveTime", JSON.stringify(newDate));
                      this.common.displayToaster('E-Way Bill LoggedIn Successfully!');
                      resolve(true);
                    }
                  }, (err) => {
                    console.log(err);
                    this.common.displayToaster("Oops, Something went wrong!");
                    this.common.hideLoader();
                    resolve(false);
                  }
                  );
                }
              }
            }
          ]
        });
        prompt.present();
      }
    });
  }

  generateEwayBill(ewayBillData) {
    (this.ewayBillLogin()).then((response) => {

      if (response) {
        const prompt = this.alertCtrl.create({
          title: "Login Success",
          message: "Generate E-Way Bill",
          buttons: [
            {
              text: "Cancel",
              cssClass: "cancelAlertButton",
              handler: data => {
                this.common.displayToaster("Process terminated!");
              }
            },
            {
              text: "Generate",
              cssClass: "submitAlertButton",
              handler: data => {
                let generateEwayBillData = {
                  userid: this.common.loginData.ID,
                  buid: this.common.selectedBusiness.buid,
                  gstinid: this.common.selectedBusiness.GSTIN.gstinid,
                  invNumber: ewayBillData.inv_no
                };
                console.log(generateEwayBillData);
                this.common.displayLoader("Please wait...");
                this.api
                  .post("generate-ewb-bill", generateEwayBillData)
                  .subscribe(
                    res => {
                      console.log(res);
                      this.common.hideLoader();
                      if (res["isSuccess"] == true) {
                        this.common.displayToaster(
                          "Bill Generated Successfully!"
                        );
                        const prompt = this.alertCtrl.create({
                          title: "Bill Generated Successfully!",
                          message: `E-Way Bill No: ${res['ewayBillNo']}<br>E-Way Bill Date: ${res['ewayBillDate']}`,
                          buttons: [
                            {
                              text: "OK",
                              cssClass: "cancelAlertButton",
                              handler: data => {
                                this.generatedBillList();
                              }
                            }
                          ]
                        });
                        prompt.present();
                      } else {
                        this.common.displayToaster(res["error"]);
                      }
                    },
                    err => {
                      console.log(err);
                      this.common.displayToaster(
                        "Oops, Something went wrong!"
                      );
                      this.common.hideLoader();
                    }
                  );
              }
            }
          ]
        });
        prompt.present();
      }
    });
  }

  cancelEwayBill(ewayBillData) {
    console.log(ewayBillData);
    const prompt = this.alertCtrl.create({
      title: "Cancel E-Way Bill",
      message: "Are you sure to cancel eway bill?",
      buttons: [
        {
          text: "No",
          cssClass: "cancelAlertButton",
          handler: data => { }
        },
        {
          text: "Yes",
          cssClass: "submitAlertButton",
          handler: data => {
            (this.ewayBillLogin()).then((response) => {
              if (response) {
                const prompt = this.alertCtrl.create({
                  title: "Login Success",
                  message: "We need few details for cancelling eway bill!",
                  buttons: [
                    {
                      text: "Cancel",
                      cssClass: "cancelAlertButton",
                      handler: data => {
                        this.common.displayToaster("Process terminated!");
                      }
                    },
                    {
                      text: "Proceed",
                      cssClass: "submitAlertButton",
                      handler: data => {
                        const prompt = this.alertCtrl.create({
                          title: "Reason For Cancellation",
                          inputs: [
                            {
                              type: "radio",
                              label: "Duplicate",
                              value: "1",
                              name: "reasonCode"
                            },
                            {
                              type: "radio",
                              label: "Order Cancelled",
                              value: "2",
                              name: "reasonCode"
                            },
                            {
                              type: "radio",
                              label: "Data Entry mistake",
                              value: "3",
                              name: "reasonCode"
                            },
                            {
                              type: "radio",
                              label: "Others",
                              value: "4",
                              name: "reasonCode"
                            }
                          ],
                          buttons: [
                            {
                              text: "Cancel",
                              cssClass: "cancelAlertButton",
                              handler: data => {
                                this.common.displayToaster(
                                  "Process terminated!"
                                );
                              }
                            },
                            {
                              text: "OK",
                              cssClass: "submitAlertButton",
                              handler: data => {
                                let reasonCode = data;
                                const prompt = this.alertCtrl.create({
                                  title: "Reason Description",
                                  inputs: [
                                    {
                                      type: "text",
                                      placeholder: "Description",
                                      name: "reason"
                                    }
                                  ],
                                  buttons: [
                                    {
                                      text: "Cancel",
                                      cssClass: "cancelAlertButton",
                                      handler: data => {
                                        this.common.displayToaster(
                                          "Process terminated!"
                                        );
                                      }
                                    },
                                    {
                                      text: "OK",
                                      cssClass: "submitAlertButton",
                                      handler: data => {
                                        let reason = data.reason;
                                        let cancelEwayBillData = {
                                          userid: this.common.loginData.ID,
                                          buid: this.common.selectedBusiness.buid,
                                          gstinid: this.common.selectedBusiness.GSTIN.gstinid,
                                          invNumber: ewayBillData.inv_no,
                                          ewbNo: ewayBillData.ewayBillNo,
                                          cancelRsnCode: reasonCode,
                                          cancelRmrk: reason
                                        };
                                        console.log('Cancel Data ', cancelEwayBillData);
                                        this.common.displayLoader("Please wait...");
                                        this.api.post("cancel-ewb", cancelEwayBillData).subscribe(
                                          res => {
                                            console.log(res);
                                            this.common.hideLoader();
                                            if (res["isSuccess"] == true) {
                                              this.common.displayToaster(
                                                "E-Way Bill Cancelled Successfully!"
                                              );
                                              const prompt = this.alertCtrl.create({
                                                title: "Cancelled",
                                                message: `Your E-Way bill no. ${ewayBillData.ewayBillNo} is cancelled successfully!`,
                                                buttons: [
                                                  {
                                                    text: "OK",
                                                    cssClass: "cancelAlertButton",
                                                    handler: data => {
                                                      this.generatedBillList();
                                                    }
                                                  }
                                                ]
                                              });
                                              prompt.present();
                                            } else {
                                              this.common.displayToaster(res["error"]);
                                            }
                                          },
                                          err => {
                                            console.log(err);
                                            this.common.displayToaster(
                                              "Oops, Something went wrong!"
                                            );
                                            this.common.hideLoader();
                                          }
                                        );
                                      }
                                    }
                                  ]
                                });
                                prompt.present();
                              }
                            }
                          ]
                        });
                        prompt.present();
                      }
                    }
                  ]
                });
                prompt.present();
              }
            });
          }
        }
      ]
    });
    prompt.present();
  }

  updateVehicleDetails(ewayBillData) {
    console.log(ewayBillData);
    let data = {
      header_text: "Update Vehicle"
    };
    let updateVehicleModal = this.modalCtrl.create("VehicleUpdatePage", data);
    updateVehicleModal.onDidDismiss(updatedVehicleData => {
      console.log(updatedVehicleData);
      if (updatedVehicleData["fromState"] != undefined) {
        let reasonRem: any;
        if (updatedVehicleData.reasonCode == 1) {
          reasonRem = "Due to Break Down";
        } else if (updatedVehicleData.reasonCode == 2) {
          reasonRem = "Due to Transshipment";
        } else if (updatedVehicleData.reasonCode == 3) {
          reasonRem = "Other";
        } else if (updatedVehicleData.reasonCode == 4) {
          reasonRem = "First Time";
        }
        updatedVehicleData.ewbNo = ewayBillData.ewayBillNo;
        updatedVehicleData.reasonRem = reasonRem;
        updatedVehicleData.transDocDate = this.common.formatTransportDate(updatedVehicleData.transDocDate);

        (this.ewayBillLogin()).then((response) => {
          if (response) {
            const prompt = this.alertCtrl.create({
              title: "Login Success",
              message: "Proceed for vehicle update!",
              buttons: [
                {
                  text: "Cancel",
                  cssClass: "cancelAlertButton",
                  handler: () => {
                    this.common.displayToaster("Process terminated!");
                  }
                },
                {
                  text: "Update",
                  cssClass: "submitAlertButton",
                  handler: data => {
                    let updateEwayBillData = {
                      userid: this.common.loginData.ID,
                      buid: this.common.selectedBusiness.buid,
                      gstinid: this.common.selectedBusiness.GSTIN.gstinid,
                      invNumber: ewayBillData.inv_no,
                      usergstin: this.common.selectedBusiness.GSTIN.gstin,
                      EwbNo: ewayBillData.ewayBillNo,
                      vehicles: updatedVehicleData
                    };
                    console.log(updateEwayBillData);
                    this.common.displayLoader("Please wait...");
                    this.api.post("update-vehicle-ewb", updateEwayBillData).subscribe(
                      res => {
                        console.log(res);
                        this.common.hideLoader();
                        if (res["isSuccess"] == true) {
                          this.common.displayToaster(
                            "Vehicle Details Updated Successfully!"
                          );
                          const prompt = this.alertCtrl.create({
                            title: "Updated",
                            message: `Your vehicle details updated successfully!`,
                            buttons: [
                              {
                                text: "OK",
                                cssClass: "cancelAlertButton",
                                handler: data => {
                                  this.generatedBillList();
                                }
                              }
                            ]
                          });
                          prompt.present();
                        } else {
                          this.common.displayToaster(res["error"]);
                        }
                      },
                      err => {
                        console.log(err);
                        this.common.displayToaster("Oops, Something went wrong!");
                        this.common.hideLoader();
                      }
                    );
                  }
                }
              ]
            });
            prompt.present();
          }
        });
      }
    });
    updateVehicleModal.present();
  }

  billDetails(value) {
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let inv_no = parseFloat(value["inv_no"]);
    let isaction = "V";
    this.api
      .get(`get-ewb-details/${userid}/${buid}/${gstinid}/${inv_no}/${isaction}`)
      .subscribe(
        res => {
          if (res["status"] == false) {
            this.common.displayToaster(res["message"]);
          } else {
            let data = JSON.parse(atob(res["ewbDetails"]))[0];
            localStorage.setItem("billDetails", JSON.stringify(data));
            this.common.selected = data;
            this.common.selectedBillFrom = null;
            this.common.selectedBillTo = null;
            this.navCtrl.push("BillDetailsPage");
          }
          this.common.hideLoader();
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  generatedBillDetails(billData) {
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let inv_no = parseFloat(billData["inv_no"]);
    let isaction = "V";
    this.api
      .get(`get-ewb-details/${userid}/${buid}/${gstinid}/${inv_no}/${isaction}`)
      .subscribe(
        res => {
          if (res["status"] == false) {
            this.common.displayToaster(res["message"]);
          } else {
            let data = JSON.parse(atob(res["ewbDetails"]))[0];
            localStorage.setItem("billDetails", JSON.stringify(data));
            this.common.selected = data;
            this.navCtrl.push("GeneratedBillDetailsPage");
          }
          this.common.hideLoader();
        },
        err => {
          console.log(err);
          this.common.displayToaster("Oops, Something went wrong!");
          this.common.hideLoader();
        }
      );
  }

  updateBusiness() {
    let data = {
      title_text: "Update Business",
      business_name: this.common.selectedBusiness.business_name,
      buid: 0,
      mode: 2,
      action: "U"
    };
    let addBusinessModal = this.modalCtrl.create("AddBusinessPage", data);
    addBusinessModal.onDidDismiss(data => {
      if (data.role != "cancel") {
        let id = btoa(this.common.loginData.ID);
        let uuid = btoa((this.common.clientData)['tallyconnecterid']);
        this.common.displayLoader("Please wait, We are preparing system...");
        this.api.get(`get-business-list/${id}/${uuid}`).subscribe(
          res => {
            this.common.hideLoader();
            localStorage.setItem("businessList", atob(res["businessList"]));
            let businessList = JSON.parse(atob(res["businessList"]));
            businessList.forEach(element => {
              if (element.buid == this.common.selectedBusiness.buid) {
                let matchedData = {
                  business_name: element.business_name,
                  buid: element.buid,
                  GSTIN: null
                };
                element.GSTIN.forEach(element => {
                  if (
                    parseInt(element.gstinid) ==
                    parseInt(this.common.selectedBusiness.GSTIN.gstinid)
                  ) {
                    matchedData.GSTIN = element;
                  }
                });
                this.common.selectedBusiness = matchedData;
                localStorage.setItem(
                  "selectedBusiness",
                  JSON.stringify(matchedData)
                );
              }
            });
          },
          err => {
            console.log(err);
            this.common.displayToaster("Oops, Something went wrong!");
            this.common.hideLoader();
          }
        );
      }
    });
    addBusinessModal.present();
  }

}
