import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { Vehicle } from '../../modal/vehicle-modal';

@IonicPage()
@Component({
	selector: 'page-vehicle-update',
	templateUrl: 'vehicle-update.html',
})
export class VehicleUpdatePage {
	header_text: string = '';
	vehicle: Vehicle = {};
	maxDate: any;
	states: any = [];

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public viewCtrl: ViewController,
		public translate: TranslateService,
		public common: CommonService,
		public api: ApiService
	) {
		this.header_text = this.navParams.get('header_text');
		let date = new Date();
		this.maxDate = this.common.disableDate(date);
		let year = date.getFullYear();
		let month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
		let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
		this.vehicle.transDocDate = `${year}-${month}-${day}`;
		this.getStates();
	}

	update() {
		if (this.vehicle.fromState == null || this.vehicle.fromState == '') {
			this.common.displayToaster('Please select from state!');
			return false;
		}
		if (this.vehicle.fromPlace == null || this.vehicle.fromPlace == '') {
			this.common.displayToaster('Please enter from place!');
			return false;
		}
		if (this.vehicle.vehicleNo == null || this.vehicle.vehicleNo == '') {
			this.common.displayToaster('Please enter vehicle number!');
			return false;
		}
		if (this.vehicle.transMode == null || this.vehicle.transMode == '') {
			this.common.displayToaster('Please select transportation mode!');
			return false;
		}
		if (this.vehicle.transDocDate == null || this.vehicle.transDocDate == '') {
			this.common.displayToaster('Please select from transportation document date!');
			return false;
		}
		if (this.vehicle.transDocNo == null || this.vehicle.transDocNo == '') {
			this.common.displayToaster('Please enter from transportation document number!');
			return false;
		}
		if (this.vehicle.vehicleType == null || this.vehicle.vehicleType == '') {
			this.common.displayToaster('Please select vehicle type!');
			return false;
		}
		if (this.vehicle.reasonCode == null || this.vehicle.reasonCode == 0) {
			this.common.displayToaster('Please select reason for vehicle update!');
			return false;
		} else {
			let data = this.vehicle;
			this.viewCtrl.dismiss(data);
		}
	}

	cancel() {
		let data = {};
		this.viewCtrl.dismiss(data);
	}

	getStates() {
		this.api.get('getstate').subscribe(
			res => {
				this.states = JSON.parse(atob(res['stateList']));
				console.log(this.states);
			},
			err => {
				console.log(err);
				this.common.displayToaster('Oops, Something went wrong!');
			}
		);
	}
}
