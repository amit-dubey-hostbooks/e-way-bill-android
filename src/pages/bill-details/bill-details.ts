import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController
} from "ionic-angular";
import { TranslateService } from "./../../providers/translate";
import { CommonService } from "./../../providers/common";
import { ApiService } from "./../../providers/api";

import { Transaction } from "../../modal/transaction-modal";
import { BillFrom } from "../../modal/bill-from-modal";
import { BillTo } from "../../modal/bill-to-modal";
import { Item } from "../../modal/item-modal";
import { Transport } from "../../modal/transport-modal";

@IonicPage()
@Component({
  selector: "page-bill-details",
  templateUrl: "bill-details.html"
})
export class BillDetailsPage {
  billDetails: any;

  transaction: Transaction = {};
  billFrom: BillFrom = {};
  billTo: BillTo = {};
  items: Item = {};
  transport: Transport = {};

  transactionDetails: boolean = false;
  billFromDetails: boolean = true;
  billToDetails: boolean = true;
  itemsDetails: boolean = true;
  transportDetails: boolean = true;

  itemsList: any = [];
  tempItemList: any = [];
  states: any = [];

  fromStateCode: any = 0;
  toStateCode: any = 0;

  startAddress: any = "";
  endAddress: any = "";

  transaction_supply_type: any = "";
  transaction_sub_type: any = "";
  document_type: any = "";
  transaction_type: any = "";

  billFromStateName: any = "";
  dispatchFromStateName: any = "";
  billToStateName: any = "";
  dispatchToStateName: any = "";

  transportation_mode: any = "";
  vehicle_type: any = "";
  vehicle_number: any = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public api: ApiService,
    public common: CommonService
  ) {
    this.billDetails = JSON.parse(localStorage.getItem("billDetails"));
    this.fromStateCode = this.billDetails.fromStateCode;
    this.toStateCode = this.billDetails.toStateCode;
    this.itemsList = this.billDetails.itemList;
    console.log(this.billDetails);
  }

  showTransactionDetails() {
    this.transactionDetails = !this.transactionDetails;
  }

  showBillFromDetails() {
    this.billFromDetails = !this.billFromDetails;
  }

  showBillToDetails() {
    this.billToDetails = !this.billToDetails;
  }

  showItemDetails() {
    this.itemsDetails = !this.itemsDetails;
  }

  showTransportDetails() {
    this.transportDetails = !this.transportDetails;
  }

  updateTransaction() {
    let data = {
      header_text: "Update Transaction",
      mode: 2
    };
    let addTransactionModal = this.modalCtrl.create("AddTransactionPage", data);
    addTransactionModal.onDidDismiss(res => {
      this.transaction = res;
      if (res["transaction_supply_type"] == "O") {
        this.common.selectedBillFrom = this.common.selectedBusiness;
        this.common.selectedBillTo = null;
        this.transaction_supply_type = "Outward-Supply";
      } else if (res["transaction_supply_type"] == "I") {
        this.common.selectedBillTo = this.common.selectedBusiness;
        this.common.selectedBillFrom = null;
        this.transaction_supply_type = "Inward-Supply";
      }
      this.transactionSubType(this.transaction.transaction_sub_type);
      this.documentType(this.transaction.document_type);
      this.transactionType(this.transaction.transaction_type);
      console.log(this.transaction);
    });
    addTransactionModal.present();
  }

  updateBillFrom() {
    let data = {
      header_text: "Update Bill From",
      mode: 2
    };
    let addTransactionModal = this.modalCtrl.create("AddBillFromPage", data);
    addTransactionModal.onDidDismiss(res => {
      this.billFrom = res;
      this.fromStateCode = res.bill_from_state;
      this.startAddress = `${res.bill_from_place}, ${res.bill_from_pincode}`;
      this.states.forEach(element => {
        if (element.state_code == res.bill_from_state_code) {
          this.billFromStateName = element.state_name;
        }
        if (element.state_code == res.bill_from_state) {
          this.dispatchFromStateName = element.state_name;
        }
      });
      console.log(this.billFrom);
    });
    addTransactionModal.present();
  }

  updateBillTo() {
    let data = {
      header_text: "Update Bill To",
      mode: 2
    };
    let addTransactionModal = this.modalCtrl.create("AddBillToPage", data);
    addTransactionModal.onDidDismiss(res => {
      this.billTo = res;
      this.toStateCode = res.bill_to_state;
      this.endAddress = `${res.bill_to_place}, ${res.bill_to_pincode}`;
      this.states.forEach(element => {
        if (element.state_code == res.bill_to_state_code) {
          this.billToStateName = element.state_name;
        }
        if (element.state_code == res.bill_to_state) {
          this.dispatchToStateName = element.state_name;
        }
      });
      console.log(this.billTo);
    });
    addTransactionModal.present();
  }

  addItem() {
    if (this.fromStateCode == 0 || this.toStateCode == 0) {
      this.common.displayToaster("Please select bill from & bill to state");
      return false;
    } else {
      let data = {
        header_text: "Add Item",
        mode: 1,
        fromStateCode: this.fromStateCode,
        toStateCode: this.toStateCode
      };
      let addTransactionModal = this.modalCtrl.create("AddItemPage", data);
      addTransactionModal.onDidDismiss(res => {
        if (res.item_description != "") {
          let foundMatch = 0;
          this.itemsList.forEach(element => {
            if (element.item_id == res.item_id) {
              foundMatch = 1;
            }
          });
          if (foundMatch == 0) {
            this.itemsList.push(res);
          } else {
            this.common.displayToaster(
              "Sorry, You can not add same item more than one!"
            );
          }
        }
      });
      addTransactionModal.present();
    }
  }

  updateItem(item) {
    let data = {
      header_text: "Update Item",
      mode: 2,
      itemData: item,
      fromStateCode: this.fromStateCode,
      toStateCode: this.toStateCode
    };
    let AddItemPageModal = this.modalCtrl.create("AddItemPage", data);
    AddItemPageModal.onDidDismiss(res => {
      this.itemsList.forEach(element => {
        if (element.item_id == res.item_id) {
          element.item_id = res.item_id;
          element.itemDescription = res.itemDescription;
          element.description = res.description;
          element.hsnSac = res.hsnSac;
          element.qty = res.qty;
          element.unitofmeasurement = res.unitofmeasurement;
          element.rate = res.rate;
          element.taxablevalue1 = res.taxablevalue1;
          element.taxablerate = res.taxablerate;
          element.cessrate = res.cessrate;
          element.cgst = res.cgst;
          element.cgstRate = res.cgstRate;
          element.sgst = res.sgst;
          element.sgstRate = res.sgstRate;
          element.igst = res.igst;
          element.igstRate = res.igstRate;
          element.cessAmt = res.cessAmt;
          element.cessAdvol = res.cessAdvol;
          element.total = res.total;
        }
      });
    });
    AddItemPageModal.present();
  }

  deleteItem(item) {
    if (this.itemsList.length >= 2) {
      const alert = this.alertCtrl.create({
        title: "Confirm Delete?",
        message: "Your item will remove from list.",
        buttons: [
          {
            text: "No",
            cssClass: "cancelAlertButton",
            handler: data => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Yes",
            cssClass: "submitAlertButton",
            handler: data => {
              let index = this.itemsList.indexOf(item);
              if (index > -1) {
                this.itemsList.splice(index, 1);
              }
            }
          }
        ]
      });
      alert.present();
    } else {
      this.common.displayToaster(
        "Sorry, You can not delete all item from invoice!"
      );
    }
  }

  updateTransport() {
    if (this.fromStateCode == 0 || this.toStateCode == 0) {
      this.common.displayToaster("Please select bill from & bill to state");
      return false;
    } else {
      let data = {
        header_text: "Update Transport",
        mode: 2,
        startAddress: this.startAddress,
        endAddress: this.endAddress
      };
      let addTransportModal = this.modalCtrl.create("AddTransportPage", data);
      addTransportModal.onDidDismiss(res => {
        this.transport = res;
        if (parseInt(this.transport.transportation_mode) == 1) {
          this.transportation_mode = "By Road";
          if (this.transport.vehicle_type == "R") {
            this.vehicle_type = "Regular";
          } else {
            this.vehicle_type = "Over Dimensional Cargo";
          }
          this.vehicle_number = this.transport.vehicle_number;
        }
        if (parseInt(this.transport.transportation_mode) == 2) {
          this.transportation_mode = "By Rail";
          this.vehicle_type = "Rail";
          this.vehicle_number = "Not Available";
        }
        if (parseInt(this.transport.transportation_mode) == 3) {
          this.transportation_mode = "By Air";
          this.vehicle_type = "Air";
          this.vehicle_number = "Not Available";
        }
        if (parseInt(this.transport.transportation_mode) == 4) {
          this.transportation_mode = "By Ship";
          this.vehicle_type = "Ship";
          this.vehicle_number = "Not Available";
        }
      });
      addTransportModal.present();
    }
  }

  getStates() {
    this.api.get("getstate").subscribe(
      res => {
        this.states = JSON.parse(atob(res["stateList"]));
      },
      err => {
        console.log(err);
        this.common.displayToaster("Oops, Something went wrong!");
      }
    );
  }

  transactionSubType(value: any) {
    let transactionSubTypeList = [
      { value: 2, name: "Import" },
      { value: 12, name: "Exhibition or Fairs" },
      { value: 3, name: "Export" },
      { value: 5, name: "For Own Use" },
      { value: 4, name: "Job Work" },
      { value: 10, name: "Line Sales" },
      { value: 8, name: "Others" },
      { value: 11, name: "Recepient Not Known" },
      { value: 9, name: "SKD/CKD" },
      { value: 1, name: "Supply" },
      { value: 6, name: "Job work Returns" },
      { value: 7, name: "Sales Return" },
      { value: 8, name: "Others" },
      { value: 1, name: "Supply" }
    ];
    transactionSubTypeList.forEach(element => {
      if (element.value == value) {
        this.transaction_sub_type = element.name;
      }
    });
  }

  documentType(value: any) {
    let documentTypeList = [
      { value: "BOE", name: "Advance Receipt" },
      { value: "BIL", name: "Bills of Supply" },
      { value: "CNT", name: "Credit Note" },
      { value: "CHL", name: "Delivery Challan" },
      { value: "OTH", name: "Other" },
      { value: "INV", name: "Sales Invoice" },
      { value: "INV", name: "Purchase Invoice" }
    ];
    documentTypeList.forEach(element => {
      if (element.value == value) {
        this.document_type = element.name;
      }
    });
  }

  transactionType(value: any) {
    let transactionTypeList = [
      { value: 1, name: "Regular" },
      { value: 2, name: "Bill To - Ship To" },
      { value: 3, name: "Bill From - Dispatch From" },
      { value: 4, name: "Combination of 2 and 3" }
    ];
    transactionTypeList.forEach(element => {
      if (element.value == value) {
        this.transaction_type = element.name;
      }
    });
  }
  
  returnTransactionType(value: any) {
    if(parseInt(value)==1){
      return 'Regular';
    }
    if(parseInt(value)==2){
      return 'Bill To - Ship To';
    }
    if(parseInt(value)==3){
      return 'Bill From - Dispatch From';
    }
    if(parseInt(value)==4){
      return 'Combination of 2 and 3';
    }
  }

  save() {
    const alert = this.alertCtrl.create({
      title: "Save Invoice?",
      message: "Your changes will be saved.",
      buttons: [
        {
          text: "No",
          cssClass: "cancelAlertButton",
          handler: data => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Yes",
          cssClass: "submitAlertButton",
          handler: data => {
            let totalCgst = 0;
            let totalSgst = 0;
            let totalIgst = 0;
            let total_cess_value = 0;
            let total_taxable_value = 0;
            let total_invoice_value = 0;
            let non_add_value = 0;
            let otherValue = 0;
            this.itemsList.forEach(element => {
              totalCgst = totalCgst + parseFloat(element.cgst);
              totalSgst = totalSgst + parseFloat(element.sgst);
              totalIgst = totalIgst + parseFloat(element.igst);
              total_cess_value = total_cess_value + parseFloat(element.cessAmt);
              non_add_value = non_add_value + parseFloat(element.cessnonadvolAmt);
              total_taxable_value = total_taxable_value + parseFloat(element.taxablevalue1);
              total_invoice_value = total_invoice_value + parseFloat(element.total);
            });

            let stateOneCode = this.billFrom.bill_from_state
              ? this.billFrom.bill_from_state
              : this.billDetails.fromStateCode;
            let stateTowCode = this.billTo.bill_to_state
              ? this.billTo.bill_to_state
              : this.billDetails.toStateCode;

            let fromState = null;
            let toState = null;

            this.states.forEach(element => {
              if (element.state_code == stateOneCode) {
                fromState = element.state_name;
              }
              if (element.state_code == stateTowCode) {
                toState = element.state_name;
              }
            });

            let tempDocDate = this.common.getSplitedDate(
              this.billDetails.docDate
            );
            let tempTransDocDate = this.common.getSplitedDate(
              this.billDetails.transDocDate
            );

            if (this.transaction.document_date != undefined) {
              tempDocDate = this.common.formatDate(
                this.transaction.document_date
              );
            }

            if (this.transport.transportation_date != undefined) {
              tempTransDocDate = this.common.formatDate(
                this.transport.transportation_date
              );
            }

            let passingData = {
              userid: this.common.loginData.ID,
              buid: this.common.selectedBusiness.buid,
              gstinid: this.common.selectedBusiness.GSTIN.gstinid,
              inv_no: this.billDetails.inv_no,

              supplyType: this.transaction.transaction_supply_type ? this.transaction.transaction_supply_type : this.billDetails.supplyType,
              supplyTypeDesc: null,
              subSupplyType: this.transaction.transaction_sub_type ? this.transaction.transaction_sub_type : this.billDetails.subSupplyType1,
              othersubtype: null,
              subSupplyType1: null,

              docType: this.transaction.document_type ? this.transaction.document_type : this.billDetails.docType1,
              docType1: null,
              docNo: this.transaction.document_no ? this.transaction.document_no : this.billDetails.docNo,
              docDate: tempDocDate,

              fromGstin: this.billFrom.bill_from_gstin ? this.billFrom.bill_from_gstin : this.billDetails.fromGstin,
              fromSupply: this.billFrom.bill_from_state_code ? this.billFrom.bill_from_state_code : this.billDetails.fromSupply,
              fromSupplyDesc: null,
              fromTrdName: this.billFrom.bill_from_name ? this.billFrom.bill_from_name : this.billDetails.fromTrdName,
              fromAddr1: this.billFrom.bill_from_address_one ? this.billFrom.bill_from_address_one : this.billDetails.fromAddr1,
              fromAddr2: this.billFrom.bill_from_address_two ? this.billFrom.bill_from_address_two : this.billDetails.fromAddr2,
              fromPlace: this.billFrom.bill_from_place ? this.billFrom.bill_from_place : this.billDetails.fromPlace,
              fromPincode: this.billFrom.bill_from_pincode ? this.billFrom.bill_from_pincode : this.billDetails.fromPincode,
              fromStateCode: this.billFrom.bill_from_state ? this.billFrom.bill_from_state : this.billDetails.fromStateCode,

              toGstin: this.billTo.bill_to_gstin ? this.billTo.bill_to_gstin : this.billDetails.toGstin,
              toSupply: this.billTo.bill_to_state_code ? this.billTo.bill_to_state_code : this.billDetails.toSupply,
              toSupplyDesc: null,
              toTrdName: this.billTo.bill_to_name ? this.billTo.bill_to_name : this.billDetails.toTrdName,
              toAddr1: this.billTo.bill_to_address_one ? this.billTo.bill_to_address_one : this.billDetails.toAddr1,
              toAddr2: this.billFrom.bill_from_address_two ? this.billFrom.bill_from_address_two : this.billDetails.toAddr2,
              toPlace: this.billTo.bill_to_place ? this.billTo.bill_to_place : this.billDetails.toPlace,
              toPincode: this.billTo.bill_to_pincode ? this.billTo.bill_to_pincode : this.billDetails.toPincode,
              toStateCode: this.billTo.bill_to_state ? this.billTo.bill_to_state : this.billDetails.toStateCode,

              totalTaxableValue: total_invoice_value,
              totalValue: total_taxable_value,
              cgstValue: totalCgst,
              sgstValue: totalSgst,
              igstValue: totalIgst,
              cessValue: total_cess_value,
              nonadvolcessValue: non_add_value,
              otherValue: otherValue,

              transporterId: this.transport.gstin ? this.transport.gstin : this.billDetails.transporterId,
              transporterName: this.transport.transportion_name ? this.transport.transportion_name : this.billDetails.transporterName,
              transDocNo: this.transport.transport_doc_no ? this.transport.transport_doc_no : this.billDetails.transDocNo,
              transMode: this.transport.transportation_mode ? this.transport.transportation_mode : this.billDetails.transMode,
              transModeDesc: null,
              transDistance: this.transport.approx_distance ? this.transport.approx_distance : this.billDetails.transDistance,
              transDocDate: tempTransDocDate,
              transactionType: this.transaction.transaction_type ? this.transaction.transaction_type : this.billDetails.transactionType,

              vehicleNo: this.transport.vehicle_number ? this.transport.vehicle_number : this.billDetails.vehicleNo,
              vehicleType: this.transport.vehicle_type ? this.transport.vehicle_type : this.billDetails.vehicleType,
              isaction: "U",

              ewayBillNo: null,
              ewayBillDate: null,

              validUpto: null,
              fromStateName: fromState,
              toStateName: toState,
              userGstin: this.common.selectedBusiness.GSTIN.gstin,
              generatedName: null,
              generatedGstin: null,
              isstatus: null,
              rejectStatus: null,

              itemList: this.itemsList,
              vehicleDetaillist: null
            };

            console.log(passingData);

            this.common.displayLoader("Please wait...");
            this.api.post("add-ewb-bill", passingData).subscribe(
              res => {
                console.log(res);
                if (res["status"] == false) {
                  this.common.displayToaster(res["message"]);
                } else {
                  this.common.displayToaster("Bill Updated Successfully!");
                  this.navCtrl.pop();
                }
                this.common.hideLoader();
              },
              err => {
                console.log(err);
                this.common.displayToaster("Oops, Something went wrong!");
                this.common.hideLoader();
              }
            );
          }
        }
      ]
    });
    alert.present();
  }
}
