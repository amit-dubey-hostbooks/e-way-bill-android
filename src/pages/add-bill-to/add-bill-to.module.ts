import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddBillToPage } from './add-bill-to';

@NgModule({
  declarations: [
    AddBillToPage,
  ],
  imports: [
    IonicPageModule.forChild(AddBillToPage),
  ],
})
export class AddBillToPageModule {}
