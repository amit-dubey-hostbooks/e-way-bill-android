import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { Transport } from '../../modal/transport-modal';

@IonicPage()
@Component({
  selector: 'page-add-transport',
  templateUrl: 'add-transport.html',
})
export class AddTransportPage {

  transport: Transport = {};
  header_text: string = '';
  maxDate: any;
  mode: number = 1;

  docNumberLabel: any = 'Transport Document Number';
  vehicleType: boolean = false;
  vehicleNumber: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public translate: TranslateService, public common: CommonService, public api: ApiService) {
    let date = new Date();
    this.maxDate = this.common.disableDate(date);
    let year = date.getFullYear();
    let month = ((date.getMonth() + 1) < 10) ? `0${(date.getMonth() + 1)}` : (date.getMonth() + 1);
    let day = (date.getDate() < 10) ? `0${date.getDate()}` : (date.getDate());
    this.transport.transportation_date = `${year}-${month}-${day}`;

    this.header_text = this.navParams.get('header_text');
    this.transport.transportion_name = this.common.selectedBusiness.business_name || null;
    this.transport.gstin = this.common.selectedBusiness.GSTIN['gstin'] || null;

    this.mode = this.navParams.get('mode');
    if (this.mode == 2) {
      let data = JSON.parse(localStorage.getItem('billDetails'));
      this.transport.transportion_name = data.transporterName;
      this.transport.gstin = data.userGstin;
      this.transport.approx_distance = parseInt(data.transDistance);
      this.transport.transportation_mode = data.transMode;
      this.transport.vehicle_type = data.vehicleType;
      this.transport.vehicle_number = data.vehicleNo;
      this.transport.transport_doc_no = data.transDocNo;
      let oldDate = (data.transDocDate).split('/');
      this.transport.transportation_date = `${oldDate[2]}-${oldDate[1]}-${oldDate[0]}`;
    }
    else {
      let startAddress = this.navParams.get('startAddress');
      let endAddress = this.navParams.get('endAddress');
      if (startAddress.length > 0 && endAddress.length > 0) {
        this.calculateDistance(startAddress, endAddress);
      }
    }
  }

  calculateDistance(startAddress: any, endAddress: any) {
    this.common.displayLoader('Please wait...');
    this.api.calculateDistance(`https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${startAddress}&destinations=${endAddress}&key=AIzaSyAW3oZbwTcJAwQ0Gi4liQCymakw_hcQVdI`).subscribe((res) => {
      console.log(res);
      this.common.hideLoader();
      if (res['status'] == 'OK') {
        let rows = res['rows'][0];
        let elements = (rows.elements)[0];
        let distance = elements.distance;
        let value = (distance.value) / 1000;
        this.transport.approx_distance = value;
      }
    }, (err) => {
      console.log(err);
      this.common.hideLoader();
    });
  }

  changeTransportMode() {
    if (parseInt(this.transport.transportation_mode) == 1) {
      this.docNumberLabel = 'Transport Document Number';
      this.vehicleType = false;
      this.vehicleNumber = false;
    }
    if (parseInt(this.transport.transportation_mode) == 2) {
      this.docNumberLabel = 'RR Number';
      this.vehicleType = true;
      this.vehicleNumber = true;
      this.transport.vehicle_type = '';
      this.transport.vehicle_number = '';
      this.transport.transportion_name = 'Rail';
    }
    if (parseInt(this.transport.transportation_mode) == 3) {
      this.docNumberLabel = 'Airway Bill Number';
      this.vehicleType = true;
      this.vehicleNumber = true;
      this.transport.vehicle_type = '';
      this.transport.vehicle_number = '';
      this.transport.transportion_name = 'Air';
    }
    if (parseInt(this.transport.transportation_mode) == 4) {
      this.docNumberLabel = 'Bill of Landing Number';
      this.vehicleType = true;
      this.vehicleNumber = true;
      this.transport.vehicle_type = '';
      this.transport.vehicle_number = '';
      this.transport.transportion_name = 'Ship';
    }
  }

  cancel() {
    let data = {};
    this.viewCtrl.dismiss(data);
  }

  save() {
    const gstinRegxp = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;

    if (this.transport.transportion_name == null || this.transport.transportion_name == '') {
      this.common.displayToaster('Please enter transporter name!');
      return false;
    }
    if (this.transport.gstin == null) {
      this.common.displayToaster('Please enter transporter gstin number!');
      return false;
    }
    if ((this.transport.gstin.length != 15 || !gstinRegxp.test(this.transport.gstin))) {
      this.common.displayToaster('Please enter a valid gstin number!');
      return false;
    }
    if (this.transport.approx_distance == null) {
      this.common.displayToaster('Please enter approx distance!');
      return false;
    }
    if (this.transport.transportation_date == null) {
      this.common.displayToaster('Please enter transportation date!');
      return false;
    }
    if (this.transport.transportation_mode == null) {
      this.common.displayToaster('Please select transportation mode!');
      return false;
    }
    if (this.transport.transport_doc_no == null) {
      this.common.displayToaster('Please enter document number!');
      return false;
    }
    else {
      let data = this.transport;
      this.viewCtrl.dismiss(data);
    }
  }

}
