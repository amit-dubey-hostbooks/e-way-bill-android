import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LanguagePage } from './language';
import { TooltipsModule } from 'ionic-tooltips';

@NgModule({
  declarations: [
    LanguagePage,
  ],
  imports: [
    TooltipsModule,
    IonicPageModule.forChild(LanguagePage),
  ],
})
export class LanguagePageModule {}
