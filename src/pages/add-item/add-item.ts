import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ActionSheetController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { Item } from '../../modal/item-modal';


@IonicPage()
@Component({
  selector: 'page-add-item',
  templateUrl: 'add-item.html',
})
export class AddItemPage {

  item: Item = {};
  header_text: string = '';

  hsnCodeList: any = [];
  temphsnCodeList: any = [];

  itemsList: any = [];
  tempItemsList: any = [];

  displayItems: boolean = false;
  displayHsnBar: boolean = false;

  taxableValue: number = 0;
  cessAmount: number = 0;
  total: number = 0;
  mode: number = 1;

  initialQuantity: number = 1;
  initialRate: number = 0;
  initialTaxRate: number = 0;
  initialCessRate: number = 0;
  initialNonAddCessAmount: number = 0;
  i_s_c_gst: number = 0;
  fromStateCode: number = 0;
  toStateCode: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public actionSheetCtrl: ActionSheetController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.header_text = this.navParams.get('header_text');
    this.itemsList = JSON.parse(localStorage.getItem('itemList'));
    this.tempItemsList = JSON.parse(localStorage.getItem('itemList'));
    this.fromStateCode = this.navParams.get('fromStateCode');
    this.toStateCode = this.navParams.get('toStateCode');
    this.mode = this.navParams.get('mode');

    if (this.mode == 2) {
      let data = this.navParams.get('itemData');
      console.log('Update Item Data: ', data);
      this.item.item_id = (parseInt(data['item_id']) > 0) ? parseInt(data['item_id']) : 0;
      this.item.item_description = (data['itemDescription'] != null) ? data['itemDescription'] : 'NULL';
      this.item.item_quantity = (parseInt(data['qty']) > 0) ? parseInt(data['qty']) : 1;
      this.item.item_hsn_sac_code = (parseInt(data['hsnSac'])> 0) ? parseInt(data['hsnSac']) : 0;
      this.item.item_tax_rate = (parseFloat(data['taxablerate']) > 0) ? parseFloat(data['taxablerate']) : 0;
      this.item.item_cess_rate = (parseFloat(data['cessrate']) > 0) ? parseFloat(data['cessrate']) : 0;
      this.item.item_cess_amount = (parseFloat(data['cessAmt']) > 0) ? parseFloat(data['cessAmt']) : 0;
      this.item.item_purchase_price = (parseFloat(data['purchase_price']) > 0) ? parseFloat(data['purchase_price']) : 0;
      this.item.item_selling_price = (parseFloat(data['rate']) > 0) ? parseFloat(data['rate']) : 0;
      this.item.item_unit = (data['unitofmeasurement'] != null) ? data['unitofmeasurement'] : 'NULL';
      this.item.item_note = (data['description'] != null) ? data['description'] : 'Goods';

      this.initialQuantity = this.item.item_quantity;
      this.initialRate = this.item.item_selling_price;
      this.initialTaxRate = this.item.item_tax_rate;
      this.initialCessRate = this.item.item_cess_rate;
      this.item.item_cess_non_advol_rate = 0;
      this.initialNonAddCessAmount = 0;
      this.displayPrice();
    }

  }

  searchHsnCodes(event) {
    this.displayHsnBar = true;
    let id = event.target.value;
    if ((id.toString()).length >= 1) {
      this.api.get(`gethsn/${id}`).subscribe((res) => {
        if ((res['status'] == true) && ((JSON.parse(atob(res['hsn'])).length >= 1))) {
          this.hsnCodeList = JSON.parse(atob(res['hsn']));
          this.temphsnCodeList = JSON.parse(atob(res['hsn']));
        }
        else {
          this.hsnCodeList = [{ hsncodes: 'No Record Found!' }];
        }
      }, (err) => {
        console.log(err);
        this.common.displayToaster('Oops, Something went wrong!');
      });
    }
  }

  selectedCode(value) {
    this.displayHsnBar = false;
    this.item.item_hsn_sac_code = value.hsncodes;
  }

  searchItem(event) {
    this.displayItems = true;
    this.initializeItems();
    const val = event.target.value;
    if (val && val.trim() != '') {
      this.itemsList = this.itemsList.filter((item) => {
        return (((item.item_description).toLowerCase()).indexOf(val.toLowerCase()) > -1);
      });
    }
    if ((this.itemsList).length == 0) {
      (this.itemsList).push({ item_description: 'No Record Found!', hsn_sac_code: '' });
    }
  }

  initializeItems() {
    this.itemsList = this.tempItemsList;
  }

  selectedItem(value) {
    this.displayItems = false;
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let itemid = parseInt(value.itemid);
    let isaction = 'V';
    this.api.get(`getitemmster/${userid}/${buid}/${gstinid}/${itemid}/${isaction}`).subscribe((res) => {
      if (res['status'] == true) {
        let data = JSON.parse(atob(res['itemList']))[0];
        console.log(data);
        this.item.item_id = (parseInt(data['itemid']) > 0) ? parseInt(data['itemid']) : 0;
        this.item.item_description = (data['item_description'] != null) ? data['item_description'] : 'NULL';
        this.item.item_hsn_sac_code = (parseInt(data['hsn_sac_code']) > 0) ? parseInt(data['hsn_sac_code']) : 0;
        this.item.item_tax_rate = (parseFloat(data['tax_rate']) > 0) ? parseFloat(data['tax_rate']) : 0;
        this.item.item_cess_rate = (parseFloat(data['cess_rate']) > 0) ? parseFloat(data['cess_rate']) : 0;
        this.item.item_cess_amount = (parseFloat(data['cess_amount']) > 0) ? parseFloat(data['cess_amount']) : 0;
        this.item.item_purchase_price = (parseFloat(data['purchase_price']) > 0) ? parseFloat(data['purchase_price']) : 0;
        this.item.item_selling_price = (parseFloat(data['selling_price']) > 0) ? parseFloat(data['selling_price']) : 0;
        this.item.item_unit = (data['unit'] != null) ? data['unit'] : 'NULL';
        this.item.item_note = (data['item_notes'] != null) ? data['item_notes'] : 'Goods';

        this.initialQuantity = 1;
        this.item.item_quantity = 1;
        this.initialRate = this.item.item_selling_price;
        this.initialTaxRate = this.item.item_tax_rate;
        this.initialCessRate = this.item.item_cess_rate;
        this.item.item_cess_non_advol_rate = 0;
        this.initialNonAddCessAmount = 0;
        this.displayPrice();
      }
    }, (err) => {
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  updateQuantity(event) {
    if (event.target.value != null) {
      this.initialQuantity = event.target.value;
      this.displayPrice();
    }
  }

  updateRate(event) {
    if (event.target.value != null) {
      this.initialRate = event.target.value;
      this.displayPrice();
    }
  }

  updateTaxRate(event) {
    if (event != null) {
      this.initialTaxRate = event;
      this.displayPrice();
    }
  }

  updateCessRate(event) {
    if (event != null) {
      this.initialCessRate = event;
      this.displayPrice();
    }
  }

  updateCessNonAdvolAmount(event) {
    if (event.target.value != null) {
      this.initialNonAddCessAmount = parseInt(event.target.value);
      this.displayPrice();
    }
  }

  displayPrice() {
    this.item.item_taxable_value = this.initialQuantity * this.initialRate;
    this.i_s_c_gst = this.item.item_taxable_value * this.initialTaxRate / 100;
    this.item.item_cess_amount = this.item.item_taxable_value * this.initialCessRate / 100;
    this.item.item_cess_non_advol_amount = this.initialNonAddCessAmount;

    this.item.item_cgst = (this.toStateCode == this.fromStateCode) ? this.i_s_c_gst / 2 : 0;
    this.item.item_sgst = (this.toStateCode == this.fromStateCode) ? this.i_s_c_gst / 2 : 0;
    this.item.item_igst = (this.toStateCode == this.fromStateCode) ? 0 : this.i_s_c_gst;
    this.item.item_total = this.item.item_taxable_value + this.i_s_c_gst + this.item.item_cess_amount + this.item.item_cess_non_advol_amount;
  }

  cancel() {
    let data = {};
    this.viewCtrl.dismiss(data);
  }

  save() {
    if (this.item.item_description == null || this.item.item_description == '') {
      this.common.displayToaster('Please select item name!');
      return false;
    } if (this.item.item_hsn_sac_code == null || this.item.item_hsn_sac_code == 0) {
      this.common.displayToaster('Please select item hsn code!');
      return false;
    } if (this.item.item_quantity == null) {
      this.common.displayToaster('Please enter item quantity!');
      return false;
    } if (this.item.item_unit == null || this.item.item_unit == '') {
      this.common.displayToaster('Please select item unit!');
      return false;
    } if (this.item.item_selling_price == null) {
      this.common.displayToaster('Please enter item price!');
      return false;
    } if (this.item.item_selling_price == null) {
      this.common.displayToaster('Please enter item price!');
      return false;
    } else {
      let data = {
        item_id: this.item.item_id,
        itemDescription: this.item.item_description,
        description: this.item.item_note,
        hsnSac: this.item.item_hsn_sac_code,
        qty: this.item.item_quantity,
        unitofmeasurement: this.item.item_unit,
        rate: this.initialRate,
        taxablevalue1: this.item.item_taxable_value,
        taxablerate: this.item.item_tax_rate,
        cessrate: this.item.item_cess_rate,
        cgst: this.item.item_cgst,
        cgstRate: (this.toStateCode == this.fromStateCode) ? this.initialTaxRate / 2 : 0,
        sgst: this.item.item_sgst,
        sgstRate: (this.toStateCode == this.fromStateCode) ? this.initialTaxRate / 2 : 0,
        igst: this.item.item_igst,
        igstRate: (this.toStateCode == this.fromStateCode) ? 0 : this.initialTaxRate,
        cessAmt: this.item.item_cess_amount,
        cessAdvol: this.item.item_cess_amount,
        cessnonadvolAmt: this.item.item_cess_non_advol_amount,
        cessnonadvolrate: this.item.item_cess_non_advol_rate,
        total: this.item.item_total
      };
      this.viewCtrl.dismiss(data);
    }
  }

}
