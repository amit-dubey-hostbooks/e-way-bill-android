import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, AlertController, IonicApp } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppUpdate } from '@ionic-native/app-update';
import { AppVersion } from '@ionic-native/app-version';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CommonService } from '../providers/common';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  showedAlert: boolean;
  confirmAlert: any;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public screenOrientation: ScreenOrientation, public appUpdate: AppUpdate, public appVersion: AppVersion, public iab: InAppBrowser, public ionicApp: IonicApp, public alertCtrl: AlertController, public common: CommonService) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // Screen Orientation Start
      if (!(this.platform.is('core') || this.platform.is('mobileweb'))) {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      }
      // Screen Orientation End

      // App VersionInfo Start
      console.log('Get App Name ', this.appVersion.getAppName());
      console.log('Get App Package Name ', this.appVersion.getPackageName());
      console.log('Get App Version Code ', this.appVersion.getVersionCode());
      console.log('Get App Version Number ', this.appVersion.getVersionNumber());
      // App VersionInfo End

      // App Update Start
      const updateUrl = 'https://hostbooks.herokuapp.com/files/eway.xml';
      this.appUpdate.checkAppUpdate(updateUrl).then(() => {
        // this.confirmAlert = this.alertCtrl.create({
        //   title: "Update Available",
        //   message: `A new version of app is available!`,
        //   buttons: [
        //     {
        //       text: 'Later',
        //       cssClass: 'cancelAlertButton',
        //       handler: data => {
        //         console.log('Update cancelled!');
        //       }
        //     },
        //     {
        //       text: 'Update',
        //       cssClass: 'submitAlertButton',
        //       handler: () => {
        //         this.iab.create('https://play.google.com/store/apps/details?id=hostbooks.com.eway.bill', '_blank', 'location=no');
        //       }
        //     }
        //   ]
        // });
        // this.confirmAlert.present();
      }).catch((err) => {
        console.log('Unable to check app update: ', err);
      });
      // App Update End

      // Check Login Start
      let user = localStorage.getItem('loginData');
      let language = localStorage.getItem('language');

      if (user != null && language != null) {
        this.rootPage = 'HomePage';
      }
      else if (user == null && language != null) {
        this.rootPage = 'LoginPage';
      }
      else {
        this.rootPage = 'LanguagePage';
      }
      // Check Login End

      // Check App Close Start
      let lastTimeBackPress = 0;
      let timePeriodToExit = 2000;
      this.platform.registerBackButtonAction(() => {

        let activePortal = this.ionicApp._loadingPortal.getActive() || this.ionicApp._modalPortal.getActive() || this.ionicApp._overlayPortal.getActive();

        if (activePortal) {
          activePortal.dismiss();
        }
        else if (this.nav.canGoBack()) {
          this.nav.pop();
        }
        else {
          if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
            this.platform.exitApp();
          } else {
            this.common.displayToaster("Press back button again to exit");
            lastTimeBackPress = new Date().getTime();
          }
        }
      });
      // Check App Close End
    });
  }

  confirmExitApp() {
    this.showedAlert = true;
    this.confirmAlert = this.alertCtrl.create({
      title: "Exit",
      message: "Are you sure you want to exit from app?",
      buttons: [
        {
          text: 'No',
          cssClass: 'cancelAlertButton',
          handler: () => {
            this.showedAlert = false;
            return;
          }
        },
        {
          text: 'Yes',
          cssClass: 'submitAlertButton',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    this.confirmAlert.present();
  }
}
