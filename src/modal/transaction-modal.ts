export interface Transaction{
  transaction_supply_type?: string,
  transaction_sub_type?: string,
  document_type?: string,
  document_no?: string,
  document_date?: string,
  transaction_type?: number
}
