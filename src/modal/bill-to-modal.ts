export interface BillTo{

  bill_to_name?:string,
  bill_to_gstin?:string,
  bill_to_state?:string,
  bill_to_state_code?:string,
  bill_to_address_one?:string,
  bill_to_address_two?:string,
  bill_to_place?:string,
  bill_to_pincode?:string

}
