import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { BusinessModal } from './business-modal';

@Injectable()

export class FirebaseService {

    public businessListRef = this.firebaseDatabase.list<BusinessModal>('businesses');
    public firebaseDatabaseRef = this.firebaseDatabase.database.ref('businesses');

    constructor(public firebaseDatabase: AngularFireDatabase) { }

    getBusinessList() {
        return this.businessListRef;
    }

    addBusiness(business: BusinessModal) {
        return this.businessListRef.push(business);
    }
    
    findBusiness(email: any) {
        return this.firebaseDatabaseRef.orderByChild('email').equalTo(email).once('value');
    }

    updateBusiness(business: BusinessModal, key: any) {
        return this.businessListRef.update(key, business);
    }

    removeBusiness(business: BusinessModal) {
        return this.businessListRef.remove(business.email);
    }
}