# How To Setup?



## Install NodeJS

Download NodeJs from https://nodejs.org/en/

Check Node Version

**node -v**

Check NPM Version

**npm -v**



## Install Angular Cli

**sudo npm install -g @angular/cli**

Check Angular Version

**ng -v**



## Install Ionic Cordova

**sudo npm install -g ionic cordova**

Check Ionic Version

**ionic -v**

Check Cordova Version

**cordova -v**



## Set JAVA_HOME On Mac

**touch .bash_profile**

**open -e .bash_profile**

**export JAVA_HOME=$(/usr/libexec/java_home)**

**source .bash_profile**

**echo $JAVA_HOME**



## Set ANDROID_HOME On Mac

**export ANDROID_HOME=/Users/amitdubey/Applications/Android/sdk**

**export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools**

OR

**export PATH=${PATH}:/Applications/Android/sdk/tools**

**export PATH=${PATH}:/Applications/Android/sdk/platform-tools**

**source .bash_profile**

**echo $PATH**



## Set GRADLE_HOME On Mac

Download gradle from https://gradle.org/releases/

Extract zip file & move to desired location

**mv /Users/amitdubey/Downloads/gradle-4.9 /Users/amitdubey/Library/gradle-4.9**

**export GRADLE_HOME=/Users/amitdubey/Library/gradle-4.9**

**export PATH=$PATH:$GRADLE_HOME/bin**

**echo $GRADLE_HOME**



## Set xcodebuild On Mac

**xcode-select --print-path**

**sudo xcode-select -s /Applications/Xcode-beta.app/Contents/Developer**

**xcode-select --print-path**



## Some usefull command for development

Run Project 

**ionic serve**

Add Platform

**ionic cordova platform add android/ios**

Run On Android 

**ionic cordova run android/ios**

Run On Emulator 

**ionic cordova emulate android/ios**

Make Production Build

**ionic cordova build android/ios --prod**

Make Release Build 

**ionic cordova build android/ios --prod --release**



## Some usefull command for debugging

Check Project Information

**ionic info**

Check Cordova Requirements 

**cordova requirements**

Perform Doctor Check

**ionic doctor check**

